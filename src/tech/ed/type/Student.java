package tech.ed.type;

public class Student
{
	private String Regno;
	private String Fname;
	private String Lname;
	private double Mobile;
	private String Email;
	private String Coursename;
	private String Password;
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String regno, String fname, String lname, double mobile, String email, String coursename,String password) {
		super();
		Regno = regno;
		Fname = fname;
		Lname = lname;
		Mobile = mobile;
		Email = email;
		Coursename = coursename;
		Password = password;
	}

	public String getRegno() {
		return Regno;
	}

	public void setRegno(String regno) {
		Regno = regno;
	}

	public String getFname() {
		return Fname;
	}

	public void setFname(String fname) {
		Fname = fname;
	}

	public String getLname() {
		return Lname;
	}

	public void setLname(String lname) {
		Lname = lname;
	}

	public double getMobile() {
		return Mobile;
	}

	public void setMobile(double mobile) {
		Mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getCoursename() {
		return Coursename;
	}

	public void setCoursename(String coursename) {
		Coursename = coursename;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	@Override
	public String toString() {
		return "Student [Regno=" + Regno + ", Fname=" + Fname + ", Lname=" + Lname + ", Mobile=" + Mobile + ", Email="
				+ Email + ", Coursename=" + Coursename + ", Password=" + Password + "]";
	}
	
	

}
