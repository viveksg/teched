package tech.ed.type;

public class Course 
{
	private int course_id;
	private String cname;
	private String cduration;
	private double cprice;
	
	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Course(int course_id, String cname, String cduration, double cprice) {
		super();
		this.course_id = course_id;
		this.cname = cname;
		this.cduration = cduration;
		this.cprice = cprice;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCduration() {
		return cduration;
	}
	public void setCduration(String cduration) {
		this.cduration = cduration;
	}
	public double getCprice() {
		return cprice;
	}
	public void setCprice(double cprice) {
		this.cprice = cprice;
	}
	@Override
	public String toString() {
		return "Course [course_id=" + course_id + ", cname=" + cname + ", cduration=" + cduration + ", cprice=" + cprice
				+ "]";
	}

	
	
}
