package tech.ed.servlet;

import java.io.*;  
import java.sql.*;  
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import tech.ed.conn.CrudConnection;  
  
@WebServlet("/CourseSearch")
public class CourseSearch extends HttpServlet {  
	private static CrudConnection cn = new CrudConnection();
public void doGet(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
          
String cid=request.getParameter("searcher");    
          
try{  
  
              
PreparedStatement ps=cn.createConnection().prepareStatement("select * from coursereg where coursename=?");  
ps.setString(1,cid);  
              
out.print("<table width=50% border=1>");  
out.print("<caption>Result:</caption>");  
  
ResultSet rs=ps.executeQuery();  
              
ResultSetMetaData rsmd=rs.getMetaData();  
int total=rsmd.getColumnCount();  
out.print("<tr>");  
for(int i=1;i<=total;i++)  
{  
out.print("<th>"+rsmd.getColumnName(i)+"</th>");  
}  
out.print("</tr>");  
while(rs.next())  
{  
out.print("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td></tr>");                  
out.print("</table>");
return;
}  
out.print("Course not found");
return;
                
}
catch (Exception se) 
{
	se.printStackTrace();
	
}  
          
finally{out.close();}  
  
}  
}  