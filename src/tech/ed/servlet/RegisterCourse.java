package tech.ed.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.ed.dao.CourseDao;
import tech.ed.type.Course;

/**
 * Servlet implementation class RegisterCourse
 */
@WebServlet("/RegisterCourse")
public class RegisterCourse extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Course course = new Course();
	private static CourseDao cdao = new CourseDao();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterCourse() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		 int cid= Integer.parseInt(request.getParameter("cid"));
		 String cname= request.getParameter("cname");
		 String cdur=request.getParameter("cdur");
		 double cprice=Double.parseDouble(request.getParameter("cpi"));
		 
		 course.setCourse_id(cid);
		 course.setCname(cname);
		 course.setCduration(cdur);
		 course.setCprice(cprice);
		 
		 try {
			 cdao.registerStudent(course);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("jsp/AllCourses.jsp");
		
	}

}
