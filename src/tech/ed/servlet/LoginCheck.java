package tech.ed.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tech.ed.conn.CrudConnection;
import tech.ed.dao.LoginDao;
import tech.ed.type.Student;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet("/LoginCheck")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Student student = new Student();
	private static CrudConnection cn = new CrudConnection();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  	response.setContentType("text/html");  
	        PrintWriter out=response.getWriter();  
		String uname=request.getParameter("username");
		String upass=request.getParameter("userpass");
		
		
		if(LoginDao.validate(uname, upass,"Admin"))
		{
			out.print("Welcome"+uname);
			HttpSession session=request.getSession();  
	        session.setAttribute("name",uname);  
	        request.getRequestDispatcher("jsp/LoginSuccess.jsp").include(request, response);
		}
		else if(LoginDao.validate(uname, upass,"Student"))
		{
			out.print("Welcome Student");
			HttpSession session=request.getSession();  
	        session.setAttribute("name",uname);  
		
	        
	        request.getRequestDispatcher("jsp/CourseEnroll.jsp").include(request, response);
	        out.print("<a href='jsp/AllCourses.jsp'>List Of Courses</a>");

		}
		else
		{
			out.print("Sorry, username or password error!");  
	         request.getRequestDispatcher("jsp/Login.jsp").include(request, response);
		}
	}

}
