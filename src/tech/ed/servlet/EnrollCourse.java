package tech.ed.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tech.ed.conn.CrudConnection;
import tech.ed.type.Student;

/**
 * Servlet implementation class LoginWelcome
 */
@WebServlet("/EnrollCourse")
public class EnrollCourse extends HttpServlet {
	private static Student student = new Student();
	private static CrudConnection cn = new CrudConnection();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
       // request.getRequestDispatcher("jsp/index.jsp").include(request, response);  
          
        HttpSession session=request.getSession(false);  
        if(session!=null){  
        String name=(String)session.getAttribute("name");  
          
       // out.print("Welcome Student , Select Your Course");  
        
        String Coursename=request.getParameter("cname");
		  String INSERT_USERS_SQL="INSERT INTO StudEnroll(email,coursename)VALUES(?,?)"; 
		  int result=0;
		  try {
		  PreparedStatement preparedStatement= cn.createConnection().prepareStatement(INSERT_USERS_SQL);
		  preparedStatement.setString(1, name);
		  preparedStatement.setString(2, Coursename); 
		  System.out.println(preparedStatement); 
		  result=preparedStatement.executeUpdate();
		  } 
		  catch (SQLException e)
		  {
		  e.printStackTrace(); 
		  }
		  request.getRequestDispatcher("jsp/EnrollmentSuccess.jsp").include(request, response);
        }  
        else{  
            out.print("Please login first");  
            request.getRequestDispatcher("jsp/Login.jsp").include(request, response);  
        }  
        out.close();  
    }  
}  