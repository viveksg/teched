package tech.ed.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.ed.dao.CourseDao;
import tech.ed.dao.StudentDao;
import tech.ed.type.Course;
import tech.ed.type.Student;

/**
 * Servlet implementation class RegisterStudent
 */
@WebServlet("/RegisterStudent")
public class RegisterStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Student student = new Student();
	private static StudentDao sdao = new StudentDao(); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				response.setContentType("text/html");  
				PrintWriter out=response.getWriter();  
		 String Fname= request.getParameter("FName");
		 String Lname=request.getParameter("LName");
		 String Name=Fname+Lname;
		 double Mobile=Double.parseDouble(request.getParameter("Mobile"));
	     String Email=request.getParameter("Email");
		 //String Coursename=request.getParameter("cname");
		 String Password = request.getParameter("Password");
		 char f = Fname.charAt(0);
		 char l = Lname.charAt(0);
		 System.out.println(f+l);
		 String Regno=String.valueOf(f).toUpperCase()+String.valueOf(l).toUpperCase();
		 
		 student.setRegno(Regno);
		 student.setFname(Fname);
		 student.setLname(Lname);
		 student.setMobile(Mobile);
		 student.setEmail(Email);
	   //student.setCoursename(Coursename);
		 student.setPassword(Password);
		
		
		 
		 try {
			 sdao.registerStudent(student);
			 sdao.LoginStudent(student);
			 //out.print("Welcome "+Name+" Successfully registered in "+Coursename);
			 request.getRequestDispatcher("jsp/StudentRegistered.jsp").include(request, response);  
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
	}

}
