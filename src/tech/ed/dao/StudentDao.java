package tech.ed.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import tech.ed.conn.CrudConnection;
import tech.ed.type.Student;

public class StudentDao {

	static Connection cn;
	public  int registerStudent(Student student)  throws ClassNotFoundException
	{
		cn=CrudConnection.createConnection();
		String INSERT_USERS_SQL="INSERT INTO StudReg(Regno,Fname,Lname,Mobile,Email,Password)VALUES(?,?,?,?,?,?)";
		int result =0;
		try
			{
			PreparedStatement preparedStatement= cn.prepareStatement(INSERT_USERS_SQL);
			preparedStatement.setString(1, student.getRegno());
			preparedStatement.setString(2, student.getFname());
			preparedStatement.setString(3, student.getLname());
			preparedStatement.setDouble(4, student.getMobile());
			preparedStatement.setString(5, student.getEmail());
			preparedStatement.setString(6, student.getPassword());
			 System.out.println(preparedStatement);
			 result= preparedStatement.executeUpdate();	
			}
		catch (SQLException e) 
			{
			e.printStackTrace(); 
			}
				
		return result;
	}
	public int LoginStudent(Student student) throws ClassNotFoundException
	{
		cn=CrudConnection.createConnection();
		String INSERT_USERS_SQL="INSERT INTO Login(uname,upass,type)VALUES(?,?,?)";
		int result =0;
		try
		{
		PreparedStatement preparedStatement= cn.prepareStatement(INSERT_USERS_SQL);
		preparedStatement.setString(1, student.getEmail());
		preparedStatement.setString(2, student.getPassword());
		preparedStatement.setString(3, "Student");
		 System.out.println(preparedStatement);
		 result= preparedStatement.executeUpdate();	
		}
		catch (SQLException e) 
		{
		e.printStackTrace(); 
		}
			
	return result;
		
	}
}
