package tech.ed.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import tech.ed.conn.CrudConnection;

public class LoginDao 
{
	static Connection cn;
	public static boolean validate(String name,String pass,String type)
	{  	
		boolean status=false;  
		try
		{  
		cn=CrudConnection.createConnection();
		PreparedStatement ps=cn.prepareStatement("select * from Login where uname=? and upass=? and type=?");  
		ps.setString(1,name);  
		ps.setString(2,pass);  
		ps.setString(3, type);
		ResultSet rs=ps.executeQuery();  
		status=rs.next();  
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} 
		
		return status;  
		
	 }  
		
}  