package tech.ed.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tech.ed.conn.CrudConnection;
import tech.ed.type.Course;

public class CourseDao {
	static Connection cn;
		    private static final String SELECT_ALL_USERS = "select * from course";
	    private static final String DELETE_USERS_SQL = "delete from coursereg where courseid = ?;";
	    private static final String SELECT_USER_BY_ID = "select courseid,coursename,courseduration,courseprice from coursereg where courseid =?";
	  
	public  int registerStudent(Course course)  throws ClassNotFoundException
	{
		cn=CrudConnection.createConnection();
		String INSERT_USERS_SQL="INSERT INTO CourseReg(courseid,coursename,courseduration,courseprice)VALUES(?,?,?,?)";
		int result =0;
		try
			{
			PreparedStatement preparedStatement= cn.prepareStatement(INSERT_USERS_SQL);
			preparedStatement.setInt(1, course.getCourse_id());
			preparedStatement.setString(2, course.getCname());
			preparedStatement.setString(3, course.getCduration());
			preparedStatement.setDouble(4, course.getCprice());
			 System.out.println(preparedStatement);
			 result= preparedStatement.executeUpdate();	
			}
		catch (SQLException e) 
			{
			e.printStackTrace(); 
			}
		
				
		return result;
		
	}
	/*
	 * public Course selectUser(int id) { Course user = null;
	 * cn=CrudConnection.createConnection(); // Step 1: Establishing a Connection
	 * try (
	 * 
	 * // Step 2:Create a statement using connection object PreparedStatement
	 * preparedStatement = cn.prepareStatement(SELECT_USER_BY_ID);) {
	 * preparedStatement.setInt(1, id); System.out.println(preparedStatement); //
	 * Step 3: Execute the query or update query ResultSet rs =
	 * preparedStatement.executeQuery();
	 * 
	 * // Step 4: Process the ResultSet object. while (rs.next()) { String name =
	 * rs.getString("coursename"); String duration = rs.getString("courseduration");
	 * Double price= rs.getDouble("courseprice"); user = new Course(id, name,
	 * duration, price); } } catch (SQLException e) { printSQLException(e); } return
	 * user; } public List < Course > selectAllCourses() {
	 * 
	 * // using try-with-resources to avoid closing resources (boiler plate code)
	 * List < Course > courses = new ArrayList < > ();
	 * cn=CrudConnection.createConnection(); // Step 1: Establishing a Connection
	 * try (
	 * 
	 * // Step 2:Create a statement using connection object PreparedStatement
	 * preparedStatement = cn.prepareStatement(SELECT_ALL_USERS);) {
	 * System.out.println(preparedStatement); // Step 3: Execute the query or update
	 * query ResultSet rs = preparedStatement.executeQuery();
	 * 
	 * // Step 4: Process the ResultSet object. while (rs.next()) { int id =
	 * rs.getInt("courseid"); String coursename = rs.getString("coursename"); String
	 * duration = rs.getString("courseduration"); Double price =
	 * rs.getDouble("courseprice"); courses.add(new Course(id,coursename, duration,
	 * price)); } } catch (SQLException e) { printSQLException(e); } return courses;
	 * } public boolean deleteUser(int id) throws SQLException { boolean rowDeleted;
	 * cn=CrudConnection.createConnection(); try ( PreparedStatement statement =
	 * cn.prepareStatement(DELETE_USERS_SQL);) { statement.setInt(1, id); rowDeleted
	 * = statement.executeUpdate() > 0; } return rowDeleted; }
	 * 
	 * private void printSQLException(SQLException ex) { for (Throwable e: ex) { if
	 * (e instanceof SQLException) { e.printStackTrace(System.err);
	 * System.err.println("SQLState: " + ((SQLException) e).getSQLState());
	 * System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
	 * System.err.println("Message: " + e.getMessage()); Throwable t =
	 * ex.getCause(); while (t != null) { System.out.println("Cause: " + t); t =
	 * t.getCause(); } } } }
	 */
	
}


