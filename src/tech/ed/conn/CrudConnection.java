package tech.ed.conn;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;

public class CrudConnection
{
    public static Connection cn;
    
    public static Connection createConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            CrudConnection.cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/teched", "root", "root");
        }
        catch (SQLException | ClassNotFoundException ex2) {
            final Exception ex = null;
            final Exception cse = ex;
            System.out.println(cse);
        }
        
        return CrudConnection.cn;
    }
}