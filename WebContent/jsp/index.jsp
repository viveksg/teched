<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>

<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta charset="ISO-8859-1">
<title>HomePage</title>
</head>
<body>

<h1 align="center" >  Welcome to Online Course Portal </h1>



<div class="grid-container" align="center" style="border:1px solid Black;" class="ex4"> 
<a class="ex4" href="Login.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">LOGIN</a>
&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;


<a class="ex4" href="Studentregister.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Register</a>
</div>
</body>
</html>