<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP List Users Records</title>
</head>
<body>
    <sql:setDataSource
        var="myDS"
        driver="com.mysql.cj.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/TechEd"
        user="root" password="root"
    />
     
    <sql:query var="listUsers"   dataSource="${myDS}">
        SELECT * FROM coursereg;
    </sql:query>
    <h1 align="center"> List of Courses</h1>
     
    <div class="grid-container" align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Course</h2></caption>
            <tr>
                <th>ID</th>
                <th> Course Name</th>
                <th>Course Duration</th>
                <th>Price</th>
            </tr>
            <c:forEach var="course" items="${listUsers.rows}">
                <tr>
                    <td><c:out value="${course.courseid}" /></td>
                    <td><c:out value="${course.coursename}" /></td>
                    <td><c:out value="${course.courseduration}" /></td>
                    <td><c:out value="${course.courseprice}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <a class="ex4" href="index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>
</body>
</html>