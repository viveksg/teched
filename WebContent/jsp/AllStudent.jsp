<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student List</title>
</head>
<body>
    <sql:setDataSource
        var="myDS"
        driver="com.mysql.cj.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/TechEd"
        user="root" password="root"
    />
     
    <sql:query var="listUsers"   dataSource="${myDS}">
      SELECT StudReg.Regno,StudReg.Fname,StudReg.Lname,StudReg.Mobile,StudReg.Email,StudEnroll.CourseName
		FROM StudReg
		INNER JOIN StudEnroll ON StudReg.email=StudEnroll.email;
    </sql:query>
     <h1 align="center"> List of Registered Students</h1>
    <div  class= "grid-container" align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Student</h2></caption>
            <tr>
                <th>Registration Number</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Course Name</th>
           </tr>
            <c:forEach var="student" items="${listUsers.rows}">
                <tr>
                    <td><c:out value="${student.regno}" /></td>
                    <td><c:out value="${student.fname}" /></td>
                    <td><c:out value="${student.lname}" /></td>
                    <td><c:out value="${student.mobile}" /></td>
                    <td><c:out value="${student.email}" /></td>
                    <td><c:out value="${student.coursename}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <a class="ex4" href="index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>
</body>
</html>