<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="java.sql.*,tech.ed.conn.CrudConnection" %>
<!DOCTYPE html>
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta charset="ISO-8859-1">
<title>Enroll Course</title>
</head>
<body>
<div class="grid-container">
<form method="post" action="<%= request.getContextPath() %>/EnrollCourse" name="Enroll">  
	<table>
	<tr>
	     <td>Course name</td>
	     <%
	     	Statement st = CrudConnection.createConnection().createStatement();
	     	ResultSet rs = st.executeQuery("select coursename from CourseReg");
	     %>
	     <td><select name = "cname" id="cname" required>
	     <option value="">Select Option</option>
	      <%  while(rs.next()){ %>
	            <option><%= rs.getString(1)%></option>
	        <% } %>
	     </select></td>
	    </tr>
	
	</table>
	 <input type="submit" value="Submit" />
</form>
</div>
<a class="ex4" href="jsp/index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>
</body>
</html>