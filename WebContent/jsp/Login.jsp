<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta charset="ISO-8859-1">
<title>Login</title>
<script type="text/javascript">
function checker() {
  var un = document.forms["Logger"]["username"].value;
  var up = document.forms["Logger"]["userpass"].value;
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if (un == "") {
	  document.getElementById("alertup").innerHTML="Enter UserName Here";
    return false;
  }
  if (up == "") {
	  document.getElementById("alertun").innerHTML="Enter Password Here";
    return false;
  }
}
</script>
</head>
<body>
<h1 align="center"> Login Portal </h1>
<div class="grid-container" >
<form method="post" action="<%= request.getContextPath() %>/LoginCheck" name="Logger" onsubmit="return checker()">  
Email:<input type="text" name="username" />
<span id="alertun" style="font-family: terminal ;color: red; font-size: 10px"></span>
<br/><br/> 
Password:<input type="password" name="userpass" />
 <span id="alertup" style="font-family: terminal ;color: red; font-size: 10px"></span>
 <br/><br/>  
<input type="submit" value="Submit" />  
</form> 
</div> 
<a class="ex4" href="index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>

</body>
</html>