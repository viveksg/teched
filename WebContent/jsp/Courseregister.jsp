<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta charset="ISO-8859-1">
<title>Register Course</title>
<script type="text/javascript">
function validator() {
  var cix = document.forms["CourseReg"]["cid"].value;
  var cnx = document.forms["CourseReg"]["cname"].value;
  var cdx = document.forms["CourseReg"]["cdur"].value;
  var cpx = document.forms["CourseReg"]["cpi"].value;
  
  if (cix == "") {
	  document.getElementById("alertid").innerHTML="Enter Course ID Here";
    return false;
  }
  if (cnx == "") {
	  document.getElementById("alertcn").innerHTML="Enter Course Name Here";
    return false;
  }
  if (cdx == "") {
	  document.getElementById("alertcd").innerHTML="Enter Course Duration Here";
    return false;
  }
  if (cpx == "") {
	  document.getElementById("alertpr").innerHTML="Enter Course Price Here";
    return false;
  }
}
</script>
</head>
<body>
 <div class ="grid-container" align="center">
  <h1>Course Registration Form</h1>
  <form method="post" action="<%= request.getContextPath() %>/RegisterCourse" name="CourseReg" onsubmit="return validator()">
   <table>
    <tr> 
     <td>Course_ID</td>
     <td><input type="text" name="cid" /></td>
      <td><span id="alertid" style="font-family: terminal ;color: red; font-size: 10px"></span></td>
    </tr>
    <tr>
     <td>Course_Name</td>
     <td><input type="text" name="cname" /></td>
      <td><span id="alertcn" style="font-family: terminal ;color: red; font-size: 10px"></span></td>
    </tr>
    <tr>
     <td>Course_Duration</td>
     <td><input type="text" name="cdur" /></td>
      <td><span id="alertcd" style="font-family: terminal ;color: red; font-size: 10px"></span></td>
    </tr>
    <tr>
     <td>Course_Price</td>
     <td><input type="number" name="cpi" /></td>
      <td><span id="alertpr" style="font-family: terminal ;color: red; font-size: 10px"></span></td>
    </tr>
   </table>
   <input type="submit" value="Submit" />
  </form>
 </div>
 <a class="ex4" href="jsp/SearchCourse.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Search Course</a>
 <a class="ex4" href="jsp/deleteCourse.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Delete Course</a>
&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
  <a class="ex4" href="jsp/index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>
</body>
</html>