<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html> 
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
</head> 
<body>  
<h1 align="center"> Search Course</h1>
<div class= "grid-container">
<form action="<%= request.getContextPath() %>/CourseDelete">  
Enter Course Id:<input type="text" name="searcher" required/><br/>  
<input type="submit" value="submit"/>  
</form>  
</div>
</body>  
</html>  