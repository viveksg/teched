<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
 <%@ page import="java.sql.*,tech.ed.conn.CrudConnection" %>
<!DOCTYPE html>
<html>
<head>
<style>
a.ex1:hover, a.ex1:active {color: white;}
a.ex2:hover, a.ex2:active {font-size: 150%;}
a.ex3:hover, a.ex3:active {background: yellow;}
a.ex4:hover, a.ex4:active {font-family: monospace;}
a.ex5:visited, a.ex5:link {text-decoration: none;}
a.ex5:hover, a.ex5:active {text-decoration: underline;}
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
<meta charset="ISO-8859-1">
<title>Register Student</title>
<script type="text/javascript">
function validateForm() {
  var fnx = document.forms["RegStu"]["FName"].value;
  var lnx = document.forms["RegStu"]["LName"].value;
  var mbx = document.forms["RegStu"]["Mobile"].value;
  //var emx = document.getElementById("Email").value;
  var emx = document.forms["RegStu"]["Email"].value;
  var emx = document.forms["RegStu"]["Password"].value;
  var letters = /^[A-Za-z]+$/;
  if (fnx == "" ) {
	  document.getElementById("alertfn").innerHTML="Enter First Name Here ";
    return false;
  }
  
  
  if (lnx == "" ) {
	  document.getElementById("alertln").innerHTML="Enter Last Name Here";
    return false;
  }
  if (mbx == "" || mbx.length<5||mbx.length>12) {
	  document.getElementById("alertmob").innerHTML="Enter Mobile Number Here";
    return false;
  }
  if (emx == "") {
	  document.getElementById("alertem").innerHTML="Enter Email Here";
    return false;
  }
  if (pmx == "") {
	  document.getElementById("alertpass").innerHTML="Enter Password Here";
    return false;
  }
}
</script>
</head>
<body>
 <div  class= "grid-container" align="center">
  <h1>Student Registration Form</h1>
  <form method="post" action="<%= request.getContextPath() %>/RegisterStudent" name="RegStu" onsubmit="return validateForm()">
   <table>
    <tr>
     <td>First Name</td>
     <td><input type="text" name="FName" required ="required"/></td>
     <td><span id="alertfn" style="font-family: terminal ;color: white; font-size: 15px"></span></td>
    </tr>
    <tr>
     <td>Last Name</td>
     <td><input type="text" name="LName"/></td>
     <td><span id="alertln" style="font-family: terminal ;color:white; font-size: 15px"></span></td>
    </tr>
    <tr>
     <td>Mobile </td>
     <td><input type="number" name="Mobile"/></td>
     <td><span id="alertmob" style="font-family: terminal ;color: white; font-size: 15px"></span></td>
    </tr>
    <tr>
     <td>Email</td>
     <td><input type="Email" name="Email"/></td>
     <td><span id="alertem" style="font-family: terminal ;color: white; font-size: 15px"></span></td>
    </tr>
     <tr>
     <td>Password</td>
     <td><input type="password" name="Password"/></td>
     <td><span id="alertpass" style="font-family: terminal ;color:white; font-size: 15px"></span></td>
    </tr>
   </table>
   <input type="submit" value="Submit" />
  </form>
 </div>
 <a class="ex4" href="SearchCourse.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Search Course</a> 
  <a class="ex4" href="index.jsp" style="font-size:150%; color:black; text-decoration: none; align="center">Home</a>
</body>
</html>